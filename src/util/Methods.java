package util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class Methods {
	public static void centerFrame(int frameWidth, int frameHeight, Component c) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		c.setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth, frameHeight);
	}

	public static <T> void shuffle(T[] stuff) {
		for (int i = 0; i < stuff.length * 10; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math.random() * stuff.length));
	}

	public static void shuffle(int[] stuff) {
		for (int i = 0; i < stuff.length * 10; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math.random() * stuff.length));
	}

	public static Color toGray(Color c) {
		int num = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
		return new Color(num, num, num);
	}

	public static Color toGray(Color c, double prc) {
		int num = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
		return new Color((int) (c.getRed() * (1 - prc) + num * prc), (int) (c.getGreen() * (1 - prc) + prc * num),
				(int) (c.getBlue() * (1 - prc) + prc * num));
	}

	public static int minI(int... ints) {
		int min = Integer.MAX_VALUE;

		for (int i : ints)
			if (i < min)
				min = i;

		return min;
	}

	public static double minD(double... doubles) {
		double min = Integer.MAX_VALUE;

		for (double i : doubles)
			if (i < min)
				min = i;

		return min;
	}

	public static int maxI(int... ints) {
		int max = Integer.MIN_VALUE;

		for (int i : ints)
			if (i > max)
				max = i;

		return max;
	}

	public static double maxD(double... doubles) {
		double max = Integer.MIN_VALUE;

		for (double i : doubles)
			if (i > max)
				max = i;

		return max;
	}

	public static double cosInter(double a, double b, double x) {
		return a * (1 - (1 - Math.cos(Math.PI * x) / 2)) + b * (1 - Math.cos(Math.PI * x)) / 2;
	}

	public static double linInter(double a, double b, double x) {
		return a + (b - a) * (x - a) / (a - b);
	}

	public static Point getClosestPointOn(Rectangle rect, Point p) {
		if (rect.contains(p))
			return p;

		if (p.x - rect.x < rect.width && p.x - rect.x > 0)
			if (p.y < rect.y)
				return new Point(p.x, rect.y);
			else
				return new Point(p.x, rect.y + rect.height);

		if (p.y - rect.y < rect.height && p.y - rect.y > 0)
			if (p.x < rect.x)
				return new Point(rect.x, p.y);
			else
				return new Point(rect.x + rect.width, p.y);

		if (p.x < rect.x && p.y < rect.y)
			return new Point(rect.x, rect.y);
		if (p.x < rect.x && p.y > rect.y + rect.height)
			return new Point(rect.x, rect.y + rect.height);
		if (p.x > rect.x + rect.width && p.y < rect.y)
			return new Point(rect.x + rect.width, rect.y);
		return new Point(rect.x + rect.width, rect.y + rect.height);
	}

	public static <T> void swap(T[] stuff, int posA, int posB) {
		T inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static Color colorMeld(Color a, Color b, double ratio) {
		return new Color((int) (a.getRed() + (b.getRed() - a.getRed()) * ratio),
				(int) (a.getGreen() + (b.getGreen() - a.getGreen()) * ratio),
				(int) (a.getBlue() + (b.getBlue() - a.getBlue()) * ratio),
				(int) (a.getAlpha() + (b.getAlpha() - a.getAlpha()) * ratio));
	}

	public static Color colorMeld(Color a, Color b, double ratio, int alpha) {
		return new Color((int) (a.getRed() + (b.getRed() - a.getRed()) * ratio),
				(int) (a.getGreen() + (b.getGreen() - a.getGreen()) * ratio),
				(int) (a.getBlue() + (b.getBlue() - a.getBlue()) * ratio), alpha);
	}

	public static Point mouse() {
		return MouseInfo.getPointerInfo().getLocation();
	}

	public static void swap(int[] stuff, int posA, int posB) {
		int inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static double getRotateX(double x, double y, double angle) {
		return x * Math.cos(angle) - y * Math.sin(angle);
	}

	public static double getRotateY(double x, double y, double angle) {
		return y * Math.cos(angle) + x * Math.sin(angle);
	}

	public static int shade(int rgb, double percent) {
		// (alpha << 24)|(red << 16)|(green << 8)|blue;
		int r = (rgb >> 16) & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = (rgb >> 0) & 0xff;
		return (int) ((255 << 24) | ((int) (r * percent) << 16) | ((int) (g * percent) << 8) | (int) (b * percent));
	}

	public static Color shade(Color c, double percent) {
		return new Color((int) (c.getRed() * percent), (int) (c.getGreen() * percent), (int) (c.getBlue() * percent));
	}

	public static double pseudoRandom(int x) {
		x = (x << 13) ^ x;
		return (1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0d);
	}

	public static Color randomColor() {
		return new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
	}

	public static Color randomColor(int seed, int alpha) {
		return new Color((int) Math.abs(pseudoRandom(seed) * 255), (int) Math.abs(pseudoRandom(seed + 52) * 255),
				(int) Math.abs(pseudoRandom(seed * 5) * 255),
				alpha < 0 ? (int) Math.abs(pseudoRandom(seed * 29 + 17) * 255) : alpha);
	}

	public static <T> T[] createArray(ArrayList<T> ls, T[] ar) {
		for (int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}

	public static Object[] createObjectArray(ArrayList<? extends Object> ls, Object[] ar) {
		for (int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}

	public static <T> ArrayList<T> getList(T[] from) {
		ArrayList<T> list = new ArrayList<T>();

		for (T t : from)
			list.add(t);

		return list;
	}

	public static double max(double[][] array) {
		double max = array[0][0];
		for (double[] d : array)
			for (double number : d)
				if (number > max)
					max = number;

		return max;
	}

	public static double getMax(Collection<? extends Number> nums) {
		double max = nums.iterator().next().doubleValue();
		for (Number n : nums)
			if (n.doubleValue() > max)
				max = n.doubleValue();
		return max;

	}

	public static int getMaxLength(Map<?, ?>... maps) {
		int max = 0;
		for (Map<?, ?> map : maps)
			if (map.keySet().size() > max)
				max = map.keySet().size();
		return max;
	}

	public static double sum(double[] array) {
		double sum = 0;
		for (double d : array)
			sum += d;
		return sum;
	}

	public static void drawCenteredText(Graphics g, String s, int x, int y, int w, int h) {
		// Find the size of string s in font f in the current Graphics context
		// g.
		FontMetrics fm = g.getFontMetrics(g.getFont());
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());

		// Center text horizontally and vertically
		int p = (w - textWidth) / 2 + x;
		int q = (h - textHeight) / 2 + fm.getAscent() + y;

		g.drawString(s, p, q); // Draw the string.
	}

	public static boolean contains(int[] array, int item) {
		for (int i : array)
			if (i == item)
				return true;

		return false;
	}

	public static String getFileContents(File f) {
		try {
			StringBuilder sb = new StringBuilder();

			BufferedReader in = new BufferedReader(new FileReader(f));
			while (true) {
				String temp = in.readLine();
				if (temp == null)
					break;
				sb.append(temp + "\n");
			}
			in.close();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * http://www.dreamincode.net/code/snippet516.htm
	 */
	public static void insertionSort(int[] list, int length) {
		int firstOutOfOrder, location, temp;

		for (firstOutOfOrder = 1; firstOutOfOrder < length; firstOutOfOrder++) {
			// Starts at second term, goes until the end of the array.
			if (list[firstOutOfOrder] < list[firstOutOfOrder - 1]) {
				// If the two are out of order, we move the element to its
				// rightful place.
				temp = list[firstOutOfOrder];
				location = firstOutOfOrder;

				do { // Keep moving down the array until we find exactly where
						// it's supposed to go.
					list[location] = list[location - 1];
					location--;
				} while (location > 0 && list[location - 1] > temp);

				list[location] = temp;
			}
		}
	}

	public static Color getColor(Color color, int i) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), i);
	}

	public static boolean containsMod(int[] array, int item, int m) {
		for (int i : array)
			if (i % m == item % m)
				return true;

		return false;
	}

	public static Color clone(Color color) {
		if (color == null)
			return null;
		return new Color(color.getRed(), color.getGreen(), color.getBlue());
	}

	public static Color colorAvg(Collection<? extends Color> clrs) {
		int r = 0, g = 0, b = 0, a = 0;

		for (Color c : clrs) {
			r += c.getRed();
			g += c.getGreen();
			b += c.getBlue();
			a += c.getAlpha();
		}

		r /= clrs.size();
		g /= clrs.size();
		b /= clrs.size();
		a /= clrs.size();

		return new Color(r, g, b, a);
	}

	public static boolean close(Color c1, Color c2) {
		return (c1.getRed() - c2.getRed()) * (c1.getRed() - c2.getRed()) + (c1.getGreen() - c2.getGreen())
				* (c1.getGreen() - c2.getGreen()) + (c1.getBlue() - c2.getBlue()) * (c1.getBlue() - c2.getBlue()) < 100;
	}
}