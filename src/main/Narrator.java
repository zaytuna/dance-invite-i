package main;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import util.Methods;

public class Narrator extends JPanel implements Runnable, MouseListener {
	private static final long serialVersionUID = 995789012662744222L;
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();
	public static final Font FONT = new Font("Comic Sans MS", Font.BOLD, 30);
	public static final Font MINI_FONT = new Font("Comic Sans MS", Font.BOLD, 12);
	public static final Font STORY_FONT = new Font("Comic Sans MS", Font.ITALIC, 20);
	public static final Font FONT3 = new Font("SERIF", Font.PLAIN, 40);

	public static String[] NO = { "Please file a Rejection Application to complete this transaction",
			"Error: Lazy Programmer \"Forgot\" to implement network communications" };
	public static String[] YES = { "\\o/     Success!" };

	public static String[] TEXT = {
			/* 0 */"PRESS ME",
			/* 1 */"I make sure to move this button<br> so you listen.",
			/* 2 */"SOME people are just apt to go to town the damn thing, you know.",
			/* 3 */"... Much like some people are apt to just <br> regurgitate bad sassy writing styles",
			/* 4 */"And fonts. Comic Sans? I mean seriously.",
			/* 5 */"...",
			/* 6 */"Of course, I'm different.<br> Why?",
			/* 7 */"Because I alone avoid unintentional clich�s...",
			/* 8 */"... by making sure that I get them all <i>on purpose</i>. <br><br> I mean, when you think about it, "
					+ "isn't the aversion to clich�s the biggest clich� of them all?",
			/* 9 */"Of couse, that would ruin the whole point,<br> which is why I prefer not to think about it.",
			/* 10 */"Allow me share a few samples of my finest writing, by pushing that button repeatedly.",
			// **************************************************************************************************************8
			/* 11 */"Once upon a time, in a galaxy far, far away� <br> it was a dark and stormy night, as the princess called out to her prince:",
			/* 12 */"So we meet again, <br>face-to-face, <br>for the first time... <br>FOR THE LAST TIME!",
			/* 13 */"And so the prince said: \"Why aren't you thinking what I�m thinking? <br>What part of �Is-that-all-you-got- is-my-middle-name�"
					+ " don�t you understand? <br> You still don�t get it, do you?\"",
			/* 14 */"Then the princess unsheathed her sword and <br>killed the prince as she realized her mistake cried out:<br> \"Don�t you die on me now!"
					+ "<br> Do you have any idea who I am?" + "<br> You will fail me no more!",
			/* 15 */"And they lived happily ever after.",
			/* 16 */"A little <i>too</i> happily... or did they?",
			// ********************************************************************8
			/* 17 */"Ah yes, quality literature.",
			/* 18 */"As a result, when I say that I mean to ask you to prom, understand that "
					+ "I will be offended if I have not done it in the <BR><BR><i>most clich�d manner possible.</i><br><br>- Oliver Richardson",
			/* 19 */"Here...",
			/* 20 */"We...",
			/* 21 */"Go!",
			// ****************************************************************
			/* 22 */"The Old Puzzle Routine",
			/* 23 */"The Confused Approach",
			/* 24 */"The Recreation-By-Obfuscation System",
			/* 25 */"The Overly Attached Technique",
			/* 26 */"The Straightforward Method",
			/* 27 */"In the end though, it doesn't matter how you ask a person to prom, or how you write, or what you make your art of.",
			/* 28 */"You can use simple, dumb, or overused building blocks...",
			/* 29 */"... and still end up with a masterpiece."
	// this comment is really useful for formatting
	};

	public static String[] UNDERTEXT = {
			"Sometimes puzzles have no meaning.<br><br>And sometimes their meaning is just very... jumbled.",
			"Of course sometimes people try a little too hard to hide things.",
			"Sometimes people make it impossible to say no.",
			"But I have selected the very most overused of all asking devices. "
					+ "<br><br>Ladies and Gentlemen... put your hands together for the anti-climax:" };

	int index = 0;

	Circle yes, no, next;
	ArrayList<Circle> allButtons = new ArrayList<Circle>();

	public JLabel info = new JLabel();
	public Patternator pat = new Patternator(SCREEN.width, SCREEN.height);

	public Narrator(int w, int h) {
		setLayout(new BorderLayout());
		add(info, BorderLayout.CENTER);
		info.setFont(FONT);
		info.setForeground(Color.WHITE);
		info.setHorizontalAlignment(SwingConstants.CENTER);

		info.setText(TEXT[0]);

		setPreferredSize(new Dimension(w, h));
		setBackground(Color.BLACK);

		yes = new Circle("yes", w / 2 - 80, h + 60, 50, false);
		no = new Circle("no", w / 2 + 80, h + 60, 50, false);
		next = new Circle("->", w / 2, h - 60, 40, true, Color.DARK_GRAY);

		allButtons.add(yes);
		allButtons.add(no);
		allButtons.add(next);

		addMouseListener(this);
		new Thread(this).start();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			pat.setView(0, 0, getWidth(), getHeight());

			for (int i = 0; i < allButtons.size(); i++)
				allButtons.get(i).update();

			if (index == 25) {
				Point mouse = Methods.mouse();
				Point loc = getLocationOnScreen();
				double dx = mouse.x - loc.x - no.x, dy = mouse.y - loc.y - no.y;
				if (dx * dx + dy * dy < (no.r + 20) * (no.r + 20)) {
					switch ((int) (Math.random() * 4)) {
					case 0:
						no.goTo((int) (Math.random() * getWidth()), (int) (Math.random() * getHeight()));
						break;
					case 1:
						no.r /= 2;
					case 2:
						no.vx = -10 / (dx - no.r);
						no.vx = -10 / (dx - no.r);
					}
				} else {
					no.r += (50 - no.r) / 10;
					no.vx /= 2;
					no.vy /= 2;
				}
				no.x = Math.min(Math.max(no.x, no.r), getWidth() - no.r);
				no.y = Math.min(Math.max(no.y, no.r), getHeight() - no.r);
			}

			repaint();
		}
	}

	public void paintComponent(Graphics g3) {
		super.paintComponent(g3);
		Graphics2D g = (Graphics2D) g3;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(pat.img, 0, 0, getWidth(), getHeight(), pat.offX, pat.offY, pat.wid, pat.hei, this);

		for (int i = 0; i < allButtons.size(); i++)
			allButtons.get(i).draw(g);

		if (index >= 22 && index <= 26) {
			g.setFont(FONT3);
			g.setColor(Color.cyan);
			Methods.drawCenteredText(g, TEXT[index], 0, 0, getWidth(), 50);
			g.setFont(FONT);
			g.setColor(Color.magenta);
			Methods.drawCenteredText(g, "Prom?", 0, 50, getWidth(), 25);
		}

		if (index == 24) {
			Circle c = allButtons.get(0);
			g.setColor(Color.WHITE);
			g.setStroke(new BasicStroke(3));
			line(g, c.x - c.r / 2, c.y - (int) (c.r * Math.sqrt(3) / 2), c.x, c.y);
			line(g, c.x + c.r / 2, c.y - (int) (c.r * Math.sqrt(3) / 2), c.x, c.y);
			oval(g, c.x - c.r / 2, c.y - c.r * 3 / 2, c.r, c.r);
			oval(g, c.x + c.r * 8 / 9, c.y - c.r / 9, c.r / 3, c.r);
			oval(g, c.x - c.r * 11 / 9, c.y - c.r / 9, c.r / 3, c.r);

			line(g, c.x - c.r / 2, c.y + (int) (c.r * Math.sqrt(3) / 2), c.x - c.r * 2 / 3, c.y + c.r * 3 / 2);
			line(g, c.x + c.r / 2, c.y + (int) (c.r * Math.sqrt(3) / 2), c.x + c.r * 2 / 3, c.y + c.r * 3 / 2);

			line(g, c.x - c.r * 2 / 3, c.y + c.r * 3 / 2, c.x - c.r * 2 / 3 - c.r / 7, c.y + c.r * 3 / 2 + c.r / 7);
			line(g, c.x - c.r * 2 / 3, c.y + c.r * 3 / 2, c.x - c.r * 2 / 3, c.y + c.r * 3 / 2 + c.r / 6);
			line(g, c.x - c.r * 2 / 3, c.y + c.r * 3 / 2, c.x - c.r * 2 / 3 + c.r / 7, c.y + c.r * 3 / 2 + c.r / 7);

			line(g, c.x + c.r * 2 / 3, c.y + c.r * 3 / 2, c.x + c.r * 2 / 3 - c.r / 7, c.y + c.r * 3 / 2 + c.r / 7);
			line(g, c.x + c.r * 2 / 3, c.y + c.r * 3 / 2, c.x + c.r * 2 / 3, c.y + c.r * 3 / 2 + c.r / 6);
			line(g, c.x + c.r * 2 / 3, c.y + c.r * 3 / 2, c.x + c.r * 2 / 3 + c.r / 7, c.y + c.r * 3 / 2 + c.r / 7);
			g.setFont(FONT);
			Methods.drawCenteredText(g, "(Uh... not a prom invite or button, I swear!)", 0, getHeight() - 45,
					getWidth(), 25);
		}
	}

	private void line(Graphics2D g, double a, double b, double c, double d) {
		g.drawLine((int) a, (int) b, (int) c, (int) d);
	}

	private void oval(Graphics2D g, double a, double b, double c, double d) {
		g.drawOval((int) a, (int) b, (int) c, (int) d);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		Narrator n = new Narrator(800, 600);
		frame.add(n, BorderLayout.CENTER);
		frame.setResizable(false);
		frame.pack();
		Methods.centerFrame(frame.getWidth(), frame.getHeight(), frame);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void update() {
		if (index != 26)
			info.setText("");
		if (index < 22 || index < 0 || index >= 27)
			index = Math.abs(index) + 1;
		else if (index <= 26)
			index *= -1;

		if (index == 30)
			endgame();

		System.out.println(index);

		if (index < 0) {
			if (index != -26)
				info.setText("<html><center>" + UNDERTEXT[-22 - index] + "<center></html>");

			info.setForeground(Color.WHITE);
			info.setFont(FONT);
			allButtons.clear();
			allButtons.add(next);
			next.text = "->";
			next.c = Color.GRAY;
			next.r = 40;
			next.goTo(50, getHeight() - (int) next.r - 10);

			Patternator.Vine v = pat.new Vine(10, 10, 30, Math.random() * Math.PI / 2, 2, 1);
			v.flowerRate = 10;
			v.speed = 1;

			pat.vines.add(v);

		} else if (index <= 21 || (index < 50 && index > 26)) {
			info.setText("<html><center>" + TEXT[index] + "<center></html>");
			info.setForeground(index >= 11 && index <= 16 ? Color.GRAY : Color.WHITE);
			info.setFont(index >= 11 && index <= 16 ? STORY_FONT : FONT);

			if (index <= 18)
				next.goTo((int) (next.r + Math.random() * (getWidth() - 2 * next.r)), getHeight() - (int) next.r - 10);
			if (index == 19) {
				next.goTo(getWidth() / 2, getHeight() - 30);
				next.r = 20;
				next.c = Color.GRAY;
				next.text = "D:";
			} else if (index == 20) {
				next.goTo(getWidth() / 2, getHeight() - 60);
				next.r = 50;
				next.c = Color.YELLOW;
				next.text = "Stop!";
			} else if (index == 21) {
				next.goTo(getWidth() / 2, getHeight() - 80);
				next.r = 70;
				next.c = Color.GREEN;
				next.text = "Noooooo!";
			}
		} else if (index == 22) {// puzzle
			next.goTo(getWidth() / 2, getHeight() + 100);
			info.setText("");
			char[] punk = { '?', '!', '.', ' ', ',', '~' };
			for (int i = -1; i <= 1; i++)
				for (int j = -2; j <= 2; j++) {
					char cha = punk[(int) (Math.random() * punk.length)];
					Circle c = new Circle("Answers" + cha, getWidth() / 2 - j * 145, 30 + getHeight() / 2 - i * 145,
							62, i == 1 && j == 1, Color.RED);
					allButtons.add(c);
				}

		} else if (index == 23) {// confused
			allButtons.clear();

			for (int i = -1; i <= 1; i++)
				for (int j = -2; j <= 2; j++) {
					double theta = Math.random() * 2 * Math.PI, power = Math.random() * 3 + 2;
					allButtons.add(new Circle("Whee!", getWidth() / 2 - j * 60, getHeight() / 2 - i * 60,
							30 + (int) (Math.random() * 35), i == 1 && j == 1, i == 1 && j == 1 ? Color.RED : Methods
									.randomColor((int) (Math.random() * 999), 255), (int) (Math.sin(theta) * power),
							(int) (Math.cos(theta) * power)));
				}
		} else if (index == 24) {// obfuscation
			allButtons.clear();
			allButtons.add(new Circle("Penguin!", getWidth() / 2, getHeight() / 2 + 50, 100, true, Color.BLUE));
		} else if (index == 25) {// attached
			allButtons.clear();
			allButtons.add(yes);
			allButtons.add(no);
			yes.goTo(getWidth() / 2 - 80, getHeight() / 2);
			no.goTo(getWidth() / 2 + 80, getHeight() / 2);
		} else if (index == 26) {// straight forward
			allButtons.clear();
			allButtons.add(yes);
			allButtons.add(no);
			yes.goTo(getWidth() / 2 - 80, getHeight() - (int) yes.r - 10);
			no.goTo(getWidth() / 2 + 80, getHeight() - (int) no.r - 10);
			no.r = 50;
			yes.c = Color.MAGENTA;
			no.c = Color.CYAN;
		}
	}

	Point pressLoc;

	public void mousePressed(MouseEvent evt) {
		for (int i = 0; i < allButtons.size(); i++)
			if (allButtons.get(i).contains(evt.getX(), evt.getY()))
				allButtons.get(i).indent = true;

		pressLoc = evt.getPoint();

		if (index < 50) {
			pat.vines.clear();
			pat.clear();
		}

		repaint();
	}

	public void mouseReleased(MouseEvent evt) {
		for (int i = 0; i < allButtons.size(); i++)
			allButtons.get(i).indent = false;

		if (next.contains(evt.getX(), evt.getY()) && next.correct) {
			update();
		} else if (yes.contains(evt.getX(), evt.getY())) {
			if (index == 25)
				update();
			else {// index = 26
					// allButtons.clear();
				info.setText("<html><center>" + YES[(int) (Math.random() * YES.length)] + "<center></html>");
				update();
			}
		} else if (no.contains(evt.getX(), evt.getY()) && index == 26) {
			// allButtons.clear();
			info.setText("<html><center>" + NO[(int) (Math.random() * NO.length)] + "<center></html>");
			update();
		} else if (index < 50) {
			for (int i = 0; i < allButtons.size(); i++)
				if (allButtons.get(i).contains(evt.getX(), evt.getY()) && allButtons.get(i).correct)
					update();
		} else if (index == 50) {
			double theta = Math.atan2(evt.getY() - pressLoc.y, evt.getX() - pressLoc.x);
			Patternator.Vine v = pat.new Vine(evt.getX(), evt.getY(), 50, theta, 2, (evt.isMetaDown() ? 1
					: Math.random() > 0.5 ? 2 : 4));
			v.flowerRate = 5 + Math.random() * 15;
			v.speed = 2;

			pat.vines.add(v);
		}

		if (Math.random() < 0.1) {
			Patternator.Vine v = pat.new Vine(evt.getX(), evt.getY(), 9, Math.random() * Math.PI * 2, 100, 2);
			v.flowerRate = 4;
			v.speed = 0;

			pat.vines.add(v);
		}

		repaint();
	}

	public void endgame() {
		index = 50;
		allButtons.clear();

		info.setText("Click and drag");

		JFrame fr = (JFrame) SwingUtilities.windowForComponent(this);
		fr.dispose();
		fr.setLocation(0, 0);
		fr.setUndecorated(true);
		fr.setSize(SCREEN);
		fr.setVisible(true);

		pat.setView(0, 0, getWidth(), getHeight());

		pat.clear();

		Patternator.Vine v = pat.new Vine(800, 800, 90, -Math.PI / 2, 2, 4);
		v.flowerRate = 10;
		v.speed = 2;

		pat.vines.add(v);
	}

	public class Circle {
		double x, y, r, vx, vy, xTo, yTo;
		boolean correct = true;
		Color c = Color.RED;
		boolean indent = false;
		String text;

		public Circle(String str, int x, int y, int r) {
			this(str, x, y, r, true);
		}

		public boolean contains(int x2, int y2) {
			return (x2 - x) * (x2 - x) + (y2 - y) * (y2 - y) <= r * r;
		}

		public Circle(String str, int x, int y, int r, boolean b) {
			this(str, x, y, r, b, Color.RED);
		}

		public Circle(String s, int x, int y, int r, boolean b, Color c) {
			this(s, x, y, r, b, c, 0, 0);
		}

		public void update() {
			if (vx != 0 || vy != 0) {
				x += vx;
				y += vy;
				if (x <= r || x >= getWidth() - r)
					vx *= -1;
				if (y <= r || y >= getHeight() - r)
					vy *= -1;
			} else if (xTo != x || yTo != y) {
				x += (xTo - x) / 10;
				y += (yTo - y) / 10;
			}
		}

		public void moveDir(double dx, double dy) {
			vx = dx;
			vy = dy;
			xTo = x;
			yTo = y;
		}

		public void goTo(int x, int y) {
			xTo = x;
			yTo = y;
			vx = 0;
			vy = 0;
		}

		public void draw(Graphics2D g) {
			g.setColor(Methods.colorMeld(c, indent ? Color.BLACK : Color.WHITE, .6));
			g.fillOval((int) (x - (r * 10) / 9), (int) (y - (r * 10) / 9), (int) (r * 20 / 9), (int) ((r * 20) / 9));
			g.setColor(c);
			g.fillOval((int) (x - r), (int) (y - r), (int) (r * 2), (int) (r * 2));
			g.setFont(FONT);
			g.setColor(Color.black);
			Methods.drawCenteredText(g, text, (int) x, (int) y - 5, 0, 0);
		}

		public Circle(String str, int x, int y, int r, boolean b, Color c, int v, int v2) {
			this.x = x;
			this.y = y;
			this.r = r;
			this.correct = b;
			this.c = c;
			this.vx = v;
			this.vy = v2;
			this.text = str;
			xTo = x;
			yTo = y;
		}
	}
}
