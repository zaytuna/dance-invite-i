package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.image.MemoryImageSource;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;

import util.Methods;

public class Patternator extends JPanel implements Runnable {
	private static final long serialVersionUID = 6464413913011714898L;

	public int mode = 2;

	private final int WIDTH, HEIGHT;
	private int[] pixels;
	private MemoryImageSource source;
	public Image img;

	// vine variables
	ArrayList<Vine> vines = new ArrayList<Vine>();
	Shape outline;
	int offX = 0, offY = 0, wid, hei;

	public Patternator(int x, int y) {
		WIDTH = x;
		HEIGHT = y;

		pixels = new int[x * y];
		Arrays.fill(pixels, Color.BLACK.getRGB());
		source = new MemoryImageSource(x, y, pixels, 0, x);
		setBackground(Color.BLACK);

		source.setAnimated(true);
		source.setFullBufferUpdates(true);
		img = createImage(source);

		new Thread(this).start();
	}

	public void paintComponent(Graphics g2) {
		super.paintComponent(g2);

		Graphics2D g = (Graphics2D) g2;

		/*
		 * if (outline == null) {
		 * TextLayout textTl = new TextLayout("TEXT", new Font("SANS_SERIF",
		 * Font.BOLD, 300), g.getFontRenderContext());
		 * outline = textTl.getOutline(null);
		 * }
		 */

		// g.translate(20, hei / 2 + outline.getBounds().height / 2);
		// g.setClip(outline);
		g.drawImage(img, (int) (-g.getTransform().getTranslateX()) - offX, (int) (-g.getTransform().getTranslateY())
				- offY, this);
		// g.setClip(null);

		// g.setColor(Color.blue);
		// g.draw(outline);

	}

	public void fillPixel(int x, int y, int rgb) {
		if (x < WIDTH && x >= 0 && y >= 0 && y < HEIGHT)
			pixels[x + y * WIDTH] = interpolateRGB(pixels[x + y * WIDTH], rgb, (rgb >> 24 & 0xff) / 255.0D);
	}

	public void setPixel(int x, int y, int rgb) {
		if (x < WIDTH && x >= 0 && y >= 0 && y < HEIGHT)
			pixels[x + y * WIDTH] = rgb;
	}

	public static int interpolateRGB(int rgb1, int rgb2, double percent) {
		// int a1 = (rgb1 >> 24) & 0xff;
		int r1 = (rgb1 >> 16) & 0xff;
		int g1 = (rgb1 >> 8) & 0xff;
		int b1 = (rgb1 >> 0) & 0xff;

		// int a2 = (rgb2 >> 24) & 0xff;
		int r2 = (rgb2 >> 16) & 0xff;
		int g2 = (rgb2 >> 8) & 0xff;
		int b2 = (rgb2 >> 0) & 0xff;

		return (/* ((int)(a1+(a2-a1)*percent)<<16) */(255 << 24) | ((int) (r1 + (r2 - r1) * percent) << 16)
				| ((int) (g1 + (g2 - g1) * percent) << 8) | (int) (b1 + (b2 - b1) * percent));
	}

	public Color randomColor(int n, int w) {

		switch (n) {
		// ****************** GREEN *****************
		case 0: {
			int g = (int) (Math.random() * 255);
			return new Color((int) (Math.random() * g), g, (int) (Math.random() * g), 250);
		}
		// ******************* AUTUMN *******************
		case 1: {
			int r = 100 + (int) (Math.random() * 150), y = (int) (Math.random() * (r - 50));
			return new Color(r, y, (int) (Math.random() * (y * 3 / 4)));
		}
		// ******************* SPRING *******************
		default:
		case 2: {
			int g = 50 + (int) (Math.random() * 205), g2 = 50 + (int) (Math.random() * 205);
			return new Color((int) (Math.random() * Math.random() * (Math.min(g, g2))), g, g2, 100);
		}
		case 3: {
			// ******************* TOPLOLOGICAL ***********
			return Methods.randomColor(10 + (int) (w / 10), 255);
		}
		case 4: {
			// ***************** TREEE
			if (w >= 20) {
				int r = (int) (Math.random() * 50);
				return new Color((int) (63 + r), 33 + (int) (r), (int) (r));
			}
			return randomColor(0, w);
		}
		}
	}

	public void update() {
		source.newPixels(offX, offY, wid, hei);
	}

	public void setView(int a, int b, int c, int d) {
		offX = a;
		offY = b;
		wid = c;
		hei = d;
		setPreferredSize(new Dimension(c, d));
	}

	public void run() {
		int counter = 0;
		while (true) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}

			switch (mode) {
			default:
			case 0:// Pixel filling @ random
				if (counter++ % 1000 < 500)
					for (int i = 0; i < wid * hei / 100; i++)
						setPixel((int) (Math.random() * wid) + offX, (int) (Math.random() * hei + offY), 0);

			case 1:
				if (vines.isEmpty()) {
				}

				else
					for (int i = 0; i < vines.size(); i++) {
						Vine v = vines.get(i);
						v.update();
						v.draw();

						if (v.width < 0)
							vines.remove(i);
					}
			}
			update();
			repaint();

		}
	}

	public class Vine {
		Color c1, c2, c;
		double x, y, theta, width, speed = 0, flowerRate = 4;
		double ratio = 0;
		int counter = 0;
		double variance = 0;
		int colorMode;

		// index < 0 ==> Flowering

		public Vine(double x, double y, int thick, double angle, double v, int colorMode) {
			this.x = x;
			this.y = y;
			this.width = thick;
			this.theta = angle;
			this.variance = v;
			this.colorMode = colorMode;
			c1 = Color.BLACK;
			c2 = randomColor(colorMode, (int) width);
		}

		public void update() {
			counter++;
			if (width >= 0) {
				if (x <= 0 || y <= 0 || x >= WIDTH || y >= HEIGHT)
					width = -1;

				width *= 0.997;
				if (width < 1) {
					width = -1;
					return;
				}

				theta += Math.cos(counter / 20D) / (3 + Math.pow(width, 1.1));
				x += Math.cos(theta) * speed;
				y += Math.sin(theta) * speed;
				c = Methods.colorMeld(c1, c2, ratio);

				ratio += 0.01;

				if (ratio >= 1) {
					c1 = c2;
					c2 = randomColor(colorMode, (int) width);
					ratio = 0;
				}

				if (width % flowerRate < 0.1) {
					Vine v = new Vine(x, y, (int) width - 3, theta + variance * (Math.random() - 0.5), 1, colorMode);
					v.c1 = c1;
					v.ratio = ratio;
					v.flowerRate = 5 + Math.random() * 10;
					v.speed = Math.random() * 2 + 0.6;
					v.c2 = c2;
					v.counter = 0;
					vines.add(v);
				}
			}
		}

		public void draw() {
			if (width > 0 && c != null) {
				fillOval((int) x, (int) y, (int) width, (int) width, c.getRGB());
			}
		}
	}

	public void fillOval(int x, int y, int w, int h, int rgb) {
		for (int j = -h / 2; j < h / 2; j++) {// absolute y pos
			double r = h / 2 + ((j + h / 2) / (double) h) * (w / 2 - h / 2);
			for (int i = 0; r * r > i * i + j * j; i++) {// relative x pos
				fillPixel(i + x, j + y, rgb);
				fillPixel(x - i, j + y, rgb);
			}
		}
	}

	public void clear() {
		Arrays.fill(pixels, 0);
	}
}
